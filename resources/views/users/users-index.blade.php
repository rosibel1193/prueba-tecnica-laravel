@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="pull-left">
                        <h2>Users CRUD</h2>
                    </div>
                    <div class="pull-right">
                        <a class="btn btn-success" href="{{ route('users.create') }}"> Create New Users</a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">  
                    @if ($users->count() >  0)
                        <form class="form-inline col-md-10" method="GET">
                            <div class="form-group mb-2">
                                <label for="filter" class="col-sm-2 col-form-label">Filter</label>
                                <input type="text" class="form-control" id="filter" name="filter" placeholder="Filter (name,nit,email,phone)" value="{{$filter}}">
                            </div>
                            <button type="submit" class="btn btn-default mb-2">Filter</button>
                        </form>

                        <form class="form-inline col-md-2" method="GET">
                            <div class="form-group mb-2">
                                <select name="limit" id="limit" onchange="submit()">
                                    <option value=""></option>
                                    <option value="5">5</option>
                                    <option value="10">10</option>
                                    <option value="20">20</option>
                                    <option value="30">30</option>
                                </select>
                            </div>
                        </form>
                    @endif
                    </div>

                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @endif

                    <table class="table table-bordered">
                        <tr>
                            <th>@sortablelink('name', 'Name')</th>
                            <th>@sortablelink('email', 'Email')</th>
                            <th>@sortablelink('phone', 'Phone')</th>
                            <th>@sortablelink('nit', 'Nit')</th>
                            <th>@sortablelink('date_birthday', 'Date birthday')</th>
                            <th>@sortablelink('edad', 'Edad')</th>
                            <th>@sortablelink('city.name', 'City')</th>
                        </tr>
                        @if ($users->count() == 0)
                        <tr>
                            <td colspan="5">No users to display.</td>
                        </tr>
                        @endif
                        @foreach ($users as $user)
                        <tr>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->phone }}</td>
                            <td>{{ $user->nit }}</td>
                            <td>{{ $user->date_birthday }}</td>
                            <td>{{ Carbon\Carbon::parse($user->date_birthday)->age }}</td>
                            <td>{{ $user->city->name }}</td>
                            <td>
                                <a class="btn btn-info" href="{{ route('users.show',$user->id) }}">Show</a>
                                    <a class="btn btn-primary" href="{{ route('users.edit',$user->id) }}">Edit</a>
                                <form action="{{ route('users.destroy',$user->id) }}" method="POST">
                
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-danger">Delete</button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </table>
                    <div class="d-flex justify-content-center">
                        {!! $users->appends(Request::except('page'))->render() !!}
                    </div> 
                </div>
            </div>
        </div> 
    </div>
</div>
@endsection