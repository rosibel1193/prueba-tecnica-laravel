@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="pull-left">
                        <h2>Conctacts</h2>
                    </div>
                    <div class="pull-right">
                        <a class="btn btn-success" href="{{ route('contact.create') }}"> Create New Contact</a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">  
                    @if ($mails->count() > 0)
                        <form class="form-inline col-md-10" method="GET">
                            <div class="form-group mb-2">
                                <label for="filter" class="col-sm-2 col-form-label">Filter</label>
                                <input type="text" class="form-control" id="filter" name="filter" placeholder="Filter (subject,message,email)" value="{{$filter}}">
                            </div>
                            <button type="submit" class="btn btn-default mb-2">Filter</button>
                        </form>

                        <form class="form-inline col-md-2" method="GET">
                            <div class="form-group mb-2">
                                <select name="limit" id="limit" onchange="submit()">
                                    <option value=""></option>
                                    <option value="5">5</option>
                                    <option value="10">10</option>
                                    <option value="20">20</option>
                                    <option value="30">30</option>
                                </select>
                            </div>
                        </form>
                    @endif
                    </div>

                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @endif

                    <table class="table table-bordered">
                        <tr>
                            <th>@sortablelink('email', 'Email')</th>
                            <th>@sortablelink('subject', 'Subject')</th>
                            <th>@sortablelink('message', 'Message')</th>
                            <th>@sortablelink('status', 'Status')</th>
                        </tr>
                        @if ($mails->count() == 0)
                        <tr>
                            <td colspan="5">No mails to display.</td>
                        </tr>
                        @endif
                        @foreach ($mails as $mail)
                        <tr>
                            <td>{{ $mail->email }}</td>
                            <td>{{ $mail->subject }}</td>
                            <td>{{ $mail->message }}</td>
                            @if ($mail->status==1)
                            <td>No enviado</td>
                            @else
                            <td>Enviado</td>
                            @endif
                        </tr>
                        @endforeach

                    </table>
                    <div class="d-flex justify-content-center">
                        {!! $mails->appends(Request::except('page'))->render() !!}
                    </div> 
                </div>
            </div>
        </div> 
    </div>
</div>
@endsection