<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Contact;

class Job extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $fillable = ['queue','payload','attempts','reserved_at','available_at','created_at'];

    public function contacts()
    {
        return $this->hasMany(Contact::class);
    }

}
