<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;
use App\Models\User;

class Contact extends Model
{
    use HasFactory, Sortable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $fillable = ['email','subject','message','status','user_id','job_id'];

    /**
     * The attributes that are for the sort.
     *
     * @var array
     */
    public $sortable = ['email','subject','message','status'];

     /**
     *  Many to one relationship with user model.
     *
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function jobs()
    {
        return $this->belongsTo(Job::class);
    }

}
