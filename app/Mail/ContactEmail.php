<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var String
     */
    public $email;
    /**
     * @var String
     */
    public $subjetc;
    /**
     * @var String
     */
    public $message;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email,$subjetc,$message)
    {
        $this->email = $email;
        $this->subjetc = $subjetc;
        $this->message = $message;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //Se llama a la vista y se agrega el mensaje
        return $this->view('mail.contact-mail')->with('messages' , $this->message);
    }
}
