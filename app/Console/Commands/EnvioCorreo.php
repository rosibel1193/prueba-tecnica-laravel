<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Contact;
use Artisan;

class EnvioCorreo extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'envio:correo';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Comando para enviar correos en cola';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        //envia los corres en cola
        Artisan::call('queue:work --stop-when-empty');

        //Cambia el status
        $contacts= Contact::with('jobs')->get();
        foreach($contacts as $contact){
            \DB::table('contacts')->where('id' , $contact->job_id )->update(['status' => 0]);
        }
    }
}
