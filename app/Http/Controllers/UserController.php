<?php namespace App\Http\Controllers;

use App\Models\User;
use Auth;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use App\Models\Country;

class UserController extends Controller {

    /**
     * @var User
     */
    private $user;


    /**
     * @var Request
     */
    private $request;

    /**
     * @param User $user
     * @param Request $request
     */
    public function __construct(User $user, Request $request)
    {
        $this->user = $user;
        $this->request = $request;

        $this->middleware('auth', ['except' => ['show']]);
    }

    /**
     * Display a listing of the resource.
     * 
     *@return Response
     */
    public function index()
    {
         //Recibe el filtro y limit
        $filter = $this->request->query('filter');
        $limit = $this->request->input('limit');

        //si esta vacio o supera, agrega un limit por defecto
        if (empty($limit) || ($limit > 30)) {
            $limit = 20;
        }

        if (!empty($filter)) {
            //Buscar user de acuerdo al filtro
            $users = User::sortable()
                ->where('users.name', 'like', '%'.$filter.'%')
                ->Orwhere('users.nit', 'like', '%'.$filter.'%')
                ->Orwhere('users.email', 'like', '%'.$filter.'%')
                ->Orwhere('users.phone', 'like', '%'.$filter.'%')
                ->paginate($limit);
        } else {
            //Busca user
            $users = User::sortable()
                ->paginate($limit);
        }

        return view('users.users-index', compact('users','filter'));
    }

    /**
     * Display the specified resource.
     *
     * @param  User  $user
     * @return Response
     */
    public function show(User $user)
    {
        return view('users.users-show',compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  User  $user
     * @return Response
     */
    public function edit(User $user)
    {
        return view('users.users-edit',compact('user'))->with(["countries" => Country::all()]);;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
       return view('users.users-create')->with(["countries" => Country::all()]);;
    }

    /**
     * Store a newly created resource in storage.
     * 
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //Realiza la validaciones de los campos
        $dt = new Carbon();
        $before = $dt->subYears(18)->format('Y-m-d');  
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'phone' => [ 'digits:10'],
            'nit' => ['required', 'string', 'max:11'],
            'date_birthday' => [ 'date', 'before:'.$before]
        ]);

        //Crea el usuario y le asigna el rol customers
        $data=$request->all();
        User::create([ 
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'phone' => $data['phone'],
            'nit' => $data['nit'],
            'date_birthday' => $data['date_birthday'],
            'city_id' => $data['city']
        ]);
        $user->roles()->attach(2);

        $message = "Se crea el usuario ".$data['email'];

        Log::debug($message);

        return redirect()->route('users.index')->with('success','Post created successfully.');
    }

    /**
     * Update the specified resource in storage.
     * 
     * @param User $user
     * @param Request $request
     *
     * @return Response
     */
    public function update(User $user, Request $request)
    {
        //valida los campos
        $dt = new Carbon();
        $before = $dt->subYears(18)->format('Y-m-d');  
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'phone' => [ 'digits:10'],
            'date_birthday' => [ 'date', 'before:'.$before]
        ]);
        //Registra un log de cambio
        \DB::table('logs')->insert([
            'user_id'=> Auth::user()->id,
            'origin' => $user,
            'change' => implode($request->all())
        ]);
        //modifica los valores
        $user->update($request->all());

        return redirect()->route('users.index')->with('success','Post updated successfully');
    }

    /**
     *Remove the specified resource from storage.
     * 
     * @param User $usr
     * @return Response
     */
    public function destroy(User $user)
    {
        //borra el usuario
        $user->delete();
        $message = "Se elimina el usuario ".$data['email'];

        Log::debug($message);
       return redirect()->route('users.index')
                       ->with('success','post deleted successfully');
    }
}
