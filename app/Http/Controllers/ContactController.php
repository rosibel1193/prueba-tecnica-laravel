<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Contact;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\QueryException;
use App\Mail\ContactEmail;
use Mail;
use Auth;
use Illuminate\Support\Facades\Log;

class ContactController extends Controller
{

     /**
     * @var Contact
     */
    private $contact;


    /**
     * @var Request
     */
    private $request;

    /**
     * @param Contact $contact
     * @param Request $request
     */
    public function __construct(Contact $contact, Request $request)
    {
        $this->contact = $contact;
        $this->request = $request;

        $this->middleware('auth', ['except' => ['show']]);
    }

    /**
     * Index resource.
     *
     * @return Response
     */
    public function index(){
        //Recibe el filtro y limit
        $filter = $this->request->query('filter');
        $limit = $this->request->input('limit');

        //si esta vacio o supera, agrega un limit por defecto
        if (empty($limit) || ($limit > 30)) {
            $limit = 5;
        }

        if (!empty($filter)) {
            //Busca los mails de acuerdo al filtro y el usuario
            $mails = Contact::sortable()
                ->where('contacts.user_id', Auth::user()->id)
                ->where('contacts.email', 'like', '%'.$filter.'%')
                ->Orwhere('contacts.subject', 'like', '%'.$filter.'%')
                ->Orwhere('contacts.message', 'like', '%'.$filter.'%')
                ->Orwhere('contacts.status', 'like', '%'.$filter.'%')
                ->paginate($limit);
        } else {
            //Busca los mails de acuerdo al usuario
            $mails = Contact::sortable()
                ->where('contacts.user_id', Auth::user()->id)
                ->paginate($limit);
        }

        return view('contact.contact-index', compact('mails','filter'));
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
       return view('contact.contact');
    }



    protected function store(Request $request){
        //Recibe los parametros
        $datos=$request->all();

        //Agrega el correo en la cola
        Mail::to($datos['email'])
        ->queue(new ContactEmail($datos['email'],$datos['subject'],$datos['message']));
        $jobs=\DB::table('jobs')->max('id');

        //Los crea
        Contact::create(['email' =>$datos['email'],'subject' =>$datos['subject'],'message' =>$datos['message'],'status' =>1,'user_id'=>Auth::user()->id,'job_id'=>$jobs]);
        
        $messages = "Se crea el envio en cola del correo ".$datos['subject']." para ".$datos['email'];

        Log::debug($messages);

        
        //Retorna la respuesta
        return $request->wantsJson()
                    ? new JsonResponse([], 201)
                    : redirect()->route('contact')->with('success','Post updated successfully');
                        
    }
}
