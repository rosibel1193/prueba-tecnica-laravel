<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\State;
use App\Models\City;

class AjaxController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('guest');
    }

    /**
     * Función para listar los estados
     *
     * @return response
     */
    public function state(Request $request){
        $state=State::where('country_id',$request->id)->get();
        return response()->json(['data' => $state]);
    }

    /**
     * Función para listar la ciudad
     *
     * @return response
     */
    public function city(Request $request){
        $state=City::where('state_id',$request->id)->get();
        return response()->json(['data' => $state]);
    }
}
