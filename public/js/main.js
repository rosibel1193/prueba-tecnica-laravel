$(document).ready(function(){
  $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')     
    }
  }); 

  //Ajax de state
  $("#country").change(function () {
    $("#country option:selected").each(function () {
        elegido=$(this).val();
        //we will send data and recive data fom our AjaxController
        $.ajax({
          url:'http://127.0.0.1:8000/state',
          data:{id: elegido},
          type:'post',
          success: function (response) {
            //imprime las opciones
            console.log(response);
            options = '<option value=""></option>';
            forma=document.getElementById('state');
            document.getElementById('fstate').style.visibility = "visible";
            response.data.forEach(function(item,index) {
                options += '<option value="'+item.id+'">' + item.name + '</option>';
                forma.innerHTML=options;
            });
          },
          statusCode: {
            404: function() {
                alert('web not found');
            }
          },
          error:function(x,xs,xt){
              //nos dara el error si es que hay alguno
              alert('error: ' + JSON.stringify(x) +"\n error string: "+ xs + "\n error throwed: " + xt);
          }
      });
    });
  })

  // Ajax de ciudad
  $("#state").change(function () {
     $("#state option:selected").each(function () {
        elegido=$(this).val();
        $.ajax({
          url:'http://127.0.0.1:8000/city',
          data:{id: elegido},
          type:'post',
          success: function (response) {
            //imprime las opciones
            options = '<option value=""></option>';
            forma=document.getElementById('city');
            document.getElementById('fcity').style.visibility = "visible";
            response.data.forEach(function(item,index) {
                options += '<option value="'+item.id+'">' + item.name + '</option>';
                forma.innerHTML=options;
            });
          },
          statusCode: {
            404: function() {
                alert('web not found');
            }
          },
          error:function(x,xs,xt){
              //nos dara el error si es que hay alguno
              alert('error: ' + JSON.stringify(x) +"\n error string: "+ xs + "\n error throwed: " + xt);
          }
      });
     });
  })

});