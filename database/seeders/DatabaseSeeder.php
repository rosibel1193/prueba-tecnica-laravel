<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //Seeder de país, ciudades y estados
        $this->call(CountrySeeder::class);
        //Seeder de usuario admin y invitado
        $this->call(UserSeeder::class);
    }
}
