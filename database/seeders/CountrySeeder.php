<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Country;
use App\Models\State;
use App\Models\City;

class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // create default countries
        $countries =[["name" => "Afganistán"],["name" => "Albania"],["name" => "Alemania"],["name" => "Andorra"],["name" => "Angola"],
        ["name" => "Antigua y Barbuda"],["name" => "Arabia Saudita"],["name" => "Argelia"],["name" => "Argentina"],["name" => "Armenia"],
        ["name" => "Australia"],["name" => "Austria"],["name" => "Azerbaiyán"],["name" => "Bahamas"],["name" => "Bangladés"],
        ["name" => "Barbados"],["name" => "Baréin"],["name" => "Bélgica"],["name" => "Belice"],["name" => "Benín"],["name" => "Bielorrusia"],
        ["name" => "Birmania"],["name" => "Bolivia"],["name" => "Bosnia y Herzegovina"],["name" => "Botsuana"],["name" => "Brasil"],
        ["name" => "Brunéi"],["name" => "Bulgaria"],["name" => "Burkina Faso"],["name" => "Burundi"],["name" => "Bután"],["name" => "Cabo Verde"],
        ["name" => "Camboya"],["name" => "Camerún"],["name" => "Canadá"],["name" => "Catar"],["name" => "Chad"],["name" => "Chile"],
        ["name" => "China"],["name" => "Chipre"],["name" => "Ciudad del Vaticano"],["name" => "Colombia"],["name" => "Comoras"],
        ["name" => "Corea del Norte"],["name" => "Corea del Sur"],["name" => "Costa de Marfil"],["name" => "Costa Rica"],["name" => "Croacia"],
        ["name" => "Cuba"],["name" => "Dinamarca"],["name" => "Dominica"],["name" => "Ecuador"],["name" => "Egipto"],["name" => "El Salvador"],
        ["name" => "Emiratos Árabes Unidos"],["name" => "Eritrea"],["name" => "Eslovaquia"],["name" => "Eslovenia"],["name" => "España"],
        ["name" => "Estados Unidos"],["name" => "Estonia"],["name" => "Etiopía"],["name" => "Filipinas"],["name" => "Finlandia"],["name" => "Fiyi"],
        ["name" => "Francia"],["name" => "Gabón"],["name" => "Gambia"],["name" => "Georgia"],["name" => "Ghana"],["name" => "Granada"],
        ["name" => "Grecia"],["name" => "Guatemala"],["name" => "Guyana"],["name" => "Guinea"],["name" => "Guinea ecuatorial"],["name" => "Guinea-Bisáu"],
        ["name" => "Haití"],["name" => "Honduras"],["name" => "Hungría"],["name" => "India"],["name" => "Indonesia"],["name" => "Irak"],
        ["name" => "Irán"],["name" => "Irlanda"],["name" => "Islandia"],["name" => "Islas Marshall"],["name" => "Islas Salomón"],["name" => "Israel"],
        ["name" => "Italia"],["name" => "Jamaica"],["name" => "Japón"],["name" => "Jordania"],["name" => "Kazajistán"],["name" => "Kenia"],
        ["name" => "Kirguistán"],["name" => "Kiribati"],["name" => "Kuwait"],["name" => "Laos"],["name" => "Lesoto"],["name" => "Letonia"],["name" => "Líbano"],
        ["name" => "Liberia"],["name" => "Libia"],["name" => "Liechtenstein"],["name" => "Lituania"],["name" => "Luxemburgo"],["name" => "Madagascar"],
        ["name" => "Malasia"],["name" => "Malaui"],["name" => "Maldivas"],["name" => "Malí"],["name" => "Malta"],["name" => "Marruecos"],
        ["name" => "Mauricio"],["name" => "Mauritania"],["name" => "México"],["name" => "Micronesia"],["name" => "Moldavia"],["name" => "Mónaco"],
        ["name" => "Mongolia"],["name" => "Montenegro"],["name" => "Mozambique"],["name" => "Namibia"],["name" => "Nauru"],["name" => "Nepal"],
        ["name" => "Nicaragua"],["name" => "Níger"],["name" => "Nigeria"],["name" => "Noruega"],["name" => "Nueva Zelanda"],["name" => "Omán"],
        ["name" => "Países Bajos"],["name" => "Pakistán"],["name" => "Palaos"],["name" => "Panamá"],["name" => "Papúa Nueva Guinea"],["name" => "Paraguay"],
        ["name" => "Perú"],["name" => "Polonia"],["name" => "Portugal"],["name" => "Reino Unido"],["name" => "República Centroafricana"],
        ["name" => "República Checa"],["name" => "República de Macedonia"],["name" => "República del Congo"],["name" => "República Democrática del Congo"],
        ["name" => "República Dominicana"],["name" => "República Sudafricana"],["name" => "Ruanda"],["name" => "Rumanía"],["name" => "Rusia"],
        ["name" => "Samoa"],["name" => "San Cristóbal y Nieves"],["name" => "San Marino"],["name" => "San Vicente y las Granadinas"],["name" => "Santa Lucía"],
        ["name" => "Santo Tomé y Príncipe"],["name" => "Senegal"],["name" => "Serbia"],["name" => "Seychelles"],["name" => "Sierra Leona"],["name" => "Singapur"],
        ["name" => "Siria"],["name" => "Somalia"],["name" => "Sri Lanka"],["name" => "Suazilandia"],["name" => "Sudán"],["name" => "Sudán del Sur"],
        ["name" => "Suecia"],["name" => "Suiza"],["name" => "Surinam"],["name" => "Tailandia"],["name" => "Tanzania"],["name" => "Tayikistán"],
        ["name" => "Timor Oriental"],["name" => "Togo"],["name" => "Tonga"],["name" => "Trinidad y Tobago"],["name" => "Túnez"],["name" => "Turkmenistán"],
        ["name" => "Turquía"],["name" => "Tuvalu"],["name" => "Ucrania"],["name" => "Uganda"],["name" => "Uruguay"],["name" => "Uzbekistán"],
        ["name" => "Vanuatu"],["name" => "Venezuela"],["name" => "Vietnam"],["name" => "Yemen"],["name" => "Yibuti"],["name" => "Zambia"],["name" => "Zimbabue"]];

        //Crea los paises
        foreach ($countries as $country) {
            if (is_null(Country::where('name', $country['name'])->first())) {
                Country::create($country);
            }
        }

       $states = [
            ["name" => 'AMAZONAS','country_id'=>'42'],
            ["name" => 'ANTIOQUIA','country_id'=>'42'],
            ["name" => 'ARAUCA','country_id'=>'42'],
            ["name" => 'ATLANTICO','country_id'=>'42'],
            ["name" => 'BOLIVAR','country_id'=>'42'],
            ["name" => 'BOYACA','country_id'=>'42'],
            ["name" => 'CALDAS','country_id'=>'42'],
            ["name" => 'CAQUETA','country_id'=>'42'],
            ["name" => 'CASANARE','country_id'=>'42'],
            ["name" => 'CAUCA','country_id'=>'42'],
            ["name" => 'CESAR','country_id'=>'42'],
            ["name" => 'CHOCO','country_id'=>'42'],
            ["name" => 'CORDOBA','country_id'=>'42'],
            ["name" => 'CUNDINAMARCA','country_id'=>'42'],
            ["name" => 'GUAINIA','country_id'=>'42'],
            ["name" => 'GUAJIRA','country_id'=>'42'],
            ["name" => 'GUAVIARE','country_id'=>'42'],
            ["name" => 'HUILA','country_id'=>'42'],
            ["name" => 'MAGDALENA','country_id'=>'42'],
            ["name" => 'META','country_id'=>'42'],
            ["name" => 'N SANTANDER','country_id'=>'42'],
            ["name" => 'NARINO','country_id'=>'42'],
            ["name" => 'PUTUMAYO','country_id'=>'42'],
            ["name" => 'QUINDIO','country_id'=>'42'],
            ["name" => 'RISARALDA','country_id'=>'42'],
            ["name" => 'SAN ANDRES','country_id'=>'42'],
            ["name" => 'SANTANDER','country_id'=>'42'],
            ["name" => 'SUCRE','country_id'=>'42'],
            ["name" => 'TOLIMA','country_id'=>'42'],
            ["name" => 'VALLE DEL CAUCA','country_id'=>'42'],
            ["name" => 'VAUPES','country_id'=>'42'],
            ["name" => 'VICHADA','country_id'=>'42']
        ]; 

        //Crea los estados
        foreach ($states as $state) {
            if (is_null(State::where('name', $state['name'])->first())) {
                State::create($state);
            }
        }

        $cities=[
            ["name" => 'CARURU',"state_id"  => 31],
            ["name" => 'MITU',"state_id"  => 31],
            ["name" => 'PACOA',"state_id"  => 31],
            ["name" => 'PAPUNAUA',"state_id"  => 31],
            ["name" => 'TARAIRA',"state_id"  => 31],
            ["name" => 'YAVARATE',"state_id"  => 31],
            ["name" => 'CUMARIBO',"state_id"  => 32],
            ["name" => 'LA PRIMAVERA',"state_id"  => 32],
            ["name" => 'PUERTO CARRENO',"state_id"  => 32],
            ["name" => 'SANTA ROSALIA',"state_id"  => 32], 
        ];

        //Crea las ciudades
        foreach ($cities as $city) {
            if (is_null(City::where('name', $city['name'])->first())) {
                City::create($city);
            }
        }
    }
}
