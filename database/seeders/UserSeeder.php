<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Role;
use Faker\Generator as Faker;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

class UserSeeder extends Seeder
{
    /**
     * @var User
     */
    private $user;

    /**
     * @var Faker
     */
    private $faker;

    /**
     * @var Role
     */
    private $role;

    /**
     * @param User $user
     * @param Role $role
     * @param Faker $faker
     */
    public function __construct(User $user, Role $role, Faker $faker)
    {
        $this->user = $user;
        $this->faker = $faker;
        $this->role = $role;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->seedDemoRoles();
        $this->seedDemoUsers();
        $userIds = $this->seedDemoCustomers();
        $this->attachCustomerGroupToUsers($userIds);
    }

    /**
     * Crea los roles
     */
    private function seedDemoRoles()
    {
        $roles=[
            ['name' => 'customers'],
            ['name' => 'admin']
        ];

        foreach ($roles as $rol) {
            if (is_null( $this->role->where('name', $rol)->first())) {
                $this->role->create($rol);
            }
        }
    }

    /**
     * Crea el usuario admin
     */
    private function seedDemoUsers()
    {
        $password = Hash::make('demo');
        $agentUser = $this->user->create([
            'email' => 'agent@demo.com',
            'password' => $password,
            'name' => $this->faker->name,
            'phone' => '1234567890',
            'nit' => '12345678901',
            'date_birthday' => '1993-01-11',
            'city_id' => 1
        ]);
        $agentGroup = $this->role->where('name', 'admin')->first();
        $agentUser->roles()->attach($agentGroup->id);
    }

    /**
     * Crea varios usuarios
     * 
     * @return Collection
     */
    private function seedDemoCustomers()
    {
        $users = collect([]);
        $password = Hash::make('demo');
        $now= Carbon::now();
        $date='1993-01-11 06:00:00';
        for ($i = 0; $i <= 20; $i++) {
            $email = $i === 0 ? 'customer@demo.com' : $this->faker->email;
            $users->push([
                'email' => $email,
                'password' => $password,
                'name' => $this->faker->name,
                'phone' => '1234567890',
                'nit' => '12345678901',
                'date_birthday' => '1993-01-11',
                'city_id' => 1
            ]);
        }

        $this->user->insert($users->toArray());

        return $this->user->whereIn('email', $users->pluck('email'))->get()->pluck('id');
    }

    /**
     * Asigna el rol customers a los usuarios creados
     * 
     * @param Collection $userIds
     */
    private function attachCustomerGroupToUsers($userIds)
    {
        $customerGroup = $this->role->where('name', 'customers')->first();

        $pivot = $userIds->map(function ($id) use ($customerGroup) {
            return ['user_id' => $id, 'role_id' => $customerGroup->id];
        })->toArray();

        \DB::table('user_role')->insert($pivot);
    }
}
